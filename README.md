# VikVim

VikVim is the repository for my personal vim setup.

## Instalation
Install `Vundle`
Install `Python`

## Keybindings

| Binding      | Description                           | Modes             |
|--------------|---------------------------------------|-------------------|
| `Alt + Up`   | Move current line up                  | Insert and Normal |
| `Alt + Down` | Move current line down                | Insert and Normal |
| `Ctrl + D`   | Toggle NERDTree                       | Normal            |
| `Ctrl + F`   | Format current document.              | Normal            |
| `F11`        | Fullcsreen mode (GVim / Windows Only) | Normal            |
| `F10`        | "Clean" mode (GVim / Windows Only)    | Normal            |

## Plugins

* [ctrlpvim/ctrlp.vim](http://github.com/ctrlpvim/ctrlp.vim)
* [octol/vim-cpp-enhanced-highlight](http://github.com/octol/vim-cpp-enhanced-highlight)
* [scrooloose/nerdtree](http://github.com/scrooloose/nerdtree)
* [scrooloose/nerdtree-git-plugin](http://github.com/scrooloose/nerdtree-git-plugin)
* [tpope/vim-fugitive](http://github.com/tpope/vim-fugitive)
* [Chiel92/vim-autoformat](http://github.com/Chiel92/vim-autoformat)
* [vim-airline/vim-airline](http://github.com/vim-airline/vim-airline)
* [Github](http://github.com/octol/vim-cpp-enhanced-highlight)
* [Github](http://github.com/octol/vim-cpp-enhanced-highlight)
* [MarcWeber/vim-addon-mw-utils](http://github.com/MarcWeber/vim-addon-mw-utils)
* [tomtom/tlib_vim](http://github.com/tomtom/tlib_vim)
* [ervandew/supertab](http://github.com/ervandew/supertab)
* [kkoenig/wimproved.vim](http://github.com/kkoenig/wimproved.vim)
* [Valloric/YouCompleteMe](http://github.com/Valloric/YouCompleteMe)
* [SirVer/ultisnips](http://github.com/SirVer/ultisnips)
* [honza/vim-snippets](http://github.com/honza/vim-snippets)
* [godlygeek/tabular](http://github.com/godlygeek/tabular)
* [jiangmiao/auto-pairs](http://github.com/jiangmiao/auto-pairs)