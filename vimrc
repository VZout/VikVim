source $VIMRUNTIME/mswin.vim
behave mswin

" RTX
autocmd BufNewFile,BufRead *.rchit set filetype=glsl460
autocmd BufNewFile,BufRead *.rmiss set filetype=glsl460
autocmd BufNewFile,BufRead *.rahit set filetype=glsl460
autocmd BufNewFile,BufRead *.rgen set filetype=glsl460

set encoding=utf8
:syntax on

"" PLUG BEGIN
call plug#begin('~/.vim/plugged')

Plug 'VundleVim/Vundle.vim'

if has("gui_running")
	"Plug 'SirVer/ultisnips'
	Plug 'kkoenig/wimproved.vim'
endif

Plug 'rust-lang/rust.vim', { 'for': ['rs', 'rlib', 'rust'] }

Plug 'psliwka/vim-smoothie'
Plug 'Valloric/YouCompleteMe', { 'for': ['cpp', 'c', 'rust', 'rlib', 'rs'] }
Plug 'octol/vim-cpp-enhanced-highlight', { 'for': ['cpp', 'c'] }
Plug 'beyondmarc/glsl.vim'
Plug 'mbbill/undotree'

Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'danielbmarques/vim-ditto', { 'for': 'markdown' }
Plug 'reedes/vim-pencil', { 'for': 'markdown' }

Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Plug 'ervandew/supertab' "Used by ultisnips
"Plug 'honza/vim-snippets'
Plug 'godlygeek/tabular'
Plug 'majutsushi/tagbar'
Plug 'haya14busa/incsearch.vim'
Plug 'drmingdrmer/vim-toggle-quickfix'

Plug 'Evalir/dosbox-vim-colorscheme'

call plug#end()
"" PLUG END

" rust youcompelteme
let g:racer_experimental_completer = 1
let g:ycm_rust_src_path = 'C:/Users/Viktor/.vim/plugged/YouCompleteMe/rust-src'

""" FONT SETTINGS
" looks good but performance sucks
"set renderoptions=type:directx

if has("gui_running")
  if has("gui_gtk2")
    set guifont=Consolas\ 11
  elseif has("gui_macvim")
    set guifont=Consolas\ Regular:h11
  elseif has("gui_win32")
    set guifont=Consolas:h11:cANSI
  endif
endif

" Search options
set hlsearch
let g:incsearch#auto_nohlsearch = 1
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
map n  <Plug>(incsearch-nohl-n)
map N  <Plug>(incsearch-nohl-N)
map *  <Plug>(incsearch-nohl-*)
map #  <Plug>(incsearch-nohl-#)
map g* <Plug>(incsearch-nohl-g*)
map g# <Plug>(incsearch-nohl-g#)

" put > infront of word wrap and put the wrap on the same indentation
set bri
set showbreak=>\ 

"set makeprg=[[\ -f\ Makefile\ ]]\ &&\ make\ \\\|\\\|\ mingw32-make\ -C\ build

set laststatus=2

" Set swap, undo and backup dir location
:set directory=$HOME/.vim/swapfiles//
if !isdirectory($HOME . "/.vim/swapfiles")
    call mkdir($HOME . "/.vim/swapfiles", "p", 0700)
endif

" undotree layout
if !exists('g:undotree_WindowLayout')
    let g:undotree_WindowLayout = 2
endif
" e.g. using 'd' instead of 'days' to save some space.
if !exists('g:undotree_ShortIndicators')
    let g:undotree_ShortIndicators = 0
endif
" Save undotree
if has("persistent_undo")
    set undodir=$HOME/.vim/undofiles//
	if !isdirectory($HOME . "/.vim/undofiles")
		call mkdir($HOME . "/.vim/undofiles", "p", 0700)
	endif
    set undofile
endif

let g:AutoPairsMultilineClose = 0

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_powerline_fonts = 0
let g:airline#extensions#whitespace#checks = [ 'none' ]
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#show_splits = 0
let g:airline#extensions#tabline#show_close_button = 0
let g:airline#extensions#tabline#tab_min_count = 2 " Hide tabline when no tabs are open
let g:airline#extensions#tabline#tabs_label = '' " change to 'tabs' when enabling buffers
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_symbols.linenr = ''

" GLSL
let g:glsl_default_version = 'glsl450'
let g:glsl_file_extensions = '*.glsl,*.vsh,*.fsh,*.vert,*.tesc,*.tese,*.geom,*.frag,*.comp'

" Disable backups
set nobackup
set nowritebackup

" YCM
set completeopt-=preview
let g:ycm_max_diagnostics_to_display = 1000
let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_min_num_identifier_candidate_chars = 999

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>', '<tab>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
"let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
"let g:UltiSnipsExpandTrigger = "<tab>"
"let g:UltiSnipsJumpForwardTrigger = "<tab>"
"let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

if has("gui_running")
	colorscheme hybrid
endif

highlight Comment cterm=italic " Italic comments.
highlight Comment gui=italic " Italic comments in gui.
highlight Todo cterm=italic " Italic comments.
highlight Todo gui=italic " Italic comments in gui.

set number

" Spaces and tabs
set tabstop=4 softtabstop=0 noexpandtab shiftwidth=4
set autoindent

" GUI options
if has("gui_running")
	:set guioptions-=m  "remove menu bar
	:set guioptions-=T  "remove toolbar
	":set guioptions-=r  "remove right-hand scroll bar
	:set guioptions-=L  "remove left-hand scroll bar
endif

if !has("gui_running")
	colorscheme hybrid 
	set background=dark
	"inoremap <Char-0x07F> <BS>
	"nnoremap <Char-0x07F> <BS>
    "set term=xterm
    set t_Co=256
	highlight Normal ctermbg=NONE
	highlight nonText ctermbg=NONE
    "let &t_AB="\e[48;5;%dm"
	"let &t_AF="\e[38;5;%dm"
    set mouse=a
    set nocompatible
    inoremap <Esc>[62~ <C-X><C-E>
    inoremap <Esc>[63~ <C-X><C-Y>
    nnoremap <Esc>[62~ <C-E>
    nnoremap <Esc>[63~ <C-Y>
endif

" Set leader to ,
let mapleader=","

" Key Bindings
" Normal Mode
nnoremap <leader>t :CtrlPTag<cr>
nnoremap <leader>nospell :set nospell<cr>
nnoremap <leader>big :set guifont=Consolas:h18<cr>
nnoremap <leader>sml :set guifont=Consolas:h11<cr>
nnoremap <leader>wrt :Limelight!!<cr>:Goyo<cr>:SoftPencil<cr>
nnoremap <leader>dos :colorscheme dosbox<cr>:set guifont=Terminal:h11<cr>:set laststatus=0<cr>
nnoremap <leader>dhc :colorscheme dosbox-black<cr>:set guifont=Terminal:h11<cr>:set laststatus=0<cr>
nnoremap <Leader>bg :let &background = ( &background == "dark"? "light" : "dark" )<CR>
" Toggle Undotree
nnoremap <leader>u :UndotreeToggle<cr>
" Align comments behind code.
nnoremap <leader>alc :Tabularize /\/\/<cr>
" Align assignments.
nnoremap <leader>ale :Tab /=<cr>
"remove :copen from F9 to get rid of the short open and close
nmap <F12> :WToggleClean<CR>
nmap <F11> :WToggleFullscreen<CR>
nmap <A-Up> :m .-2<cr>==
nmap <A-Down> :m .+1<cr>==
" Toggle cl (simplified quickfix) preview.
nmap <F2> <Plug>window:quickfix:loop
" Toggle quickfix window.
nmap <F1> :cl<cr>

" Move between splits using keys
noremap <C-j> <C-W>j
noremap <C-k> <C-W>k
noremap <C-h> <C-W>h
noremap <C-l> <C-W>l
noremap <C-Down> <C-W>j
noremap <C-Up> <C-W>k
noremap <C-Left> <C-W>h
noremap <C-Right> <C-W>l

" Insert Mode
inoremap <A-Up> <Esc>:m .-2<CR>==gi
inoremap <A-Down> <Esc>:m .+1<CR>==gi

" Visual Mode
vnoremap <A-Up> :m '<-2<CR>gv=gv
vnoremap <A-Down> :m '>+1<CR>gv=gv

" CTRLP options
let g:ctrlp_extensions = ['tag']
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

" ==============================
" =========== VNotes ===========
" ==============================
:let g:note_dir = "~/Documents/Notes"

" type of open (:e vs sp) should be optional
:function Note(name)
	:if a:name == ""
		:echom "Please provide a name"
		:return
	:endif

	:let filename = a:name . ".markdown"
	:cd `=g:note_dir`

	if filereadable(filename)
		:echom "Opening Existing Note\"

		:sp `=filename` " Edit/create file
	:else
		:echom "Creating New Note"

		:sp `=filename` " Edit/create file

		:let @a = "# Welcome to VNotes\nThank you for using VNote! Hopefully it will make your live just that little bit easier."

		:normal! G

		:execute "put a"

		:normal! kkdd
		:normal! ggVG
	:endif
:endfunction

:function ListNotes()
	:cd `=g:note_dir`
	:Sex
:endfunction

:command! -nargs=* Note call Note( '<args>' )
:command! NoteList call ListNotes()
"##### END VNOTE #####

" PRESENTATION MODE
let s:hidden_all = 0
function! ToggleHiddenAll()
    if s:hidden_all  == 0
        let s:hidden_all = 1
        set noshowmode
        set noruler
        set laststatus=0
        set noshowcmd
    else
        let s:hidden_all = 0
        set showmode
        set ruler
        set laststatus=2
        set showcmd
    endif
endfunction
	
let s:hidden_all = 0
function! TogglePresentationMode()
    if s:hidden_all  == 0
        let s:hidden_all = 1
        set noshowmode
        set noshowcmd
		:set guifont=Consolas:h18
		:execute "WToggleFullscreen"
		:execute ":Goyo 70%x120%"
    else
        let s:hidden_all = 0
        set showmode
        set showcmd
		:Goyo
		:set guifont=Consolas:h11
		:execute "WToggleFullscreen"
    endif
endfunction

nnoremap <leader>pres :call TogglePresentationMode()<CR>
